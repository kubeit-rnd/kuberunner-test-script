#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys
import time


def main():
    print("Hello Kube Runner from Git! Now I'm going to sleep for 10 seconds.")
    time.sleep(10)
    print("I'm here again! Bye :)")

    return 0


if __name__ == "__main__":
    sys.exit(main())
